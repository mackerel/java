# Copyright 2009-2014 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require ant java

S_PN="${PN/java/j}"
MY_PV=$(ever replace_all _ )
MY_PNV=${S_PN}_${MY_PV}

SUMMARY="Implements the XSLT XML transformation and the XPath language for Java"
DESCRIPTION="
Xalan-Java is an XSLT processor for transforming XML documents into HTML, text,
or other XML document types. It implements XSL Transformations (XSLT) Version
1.0 and XML Path Language (XPath) Version 1.0 and can be used from the command
line, in an applet or a servlet, or as a module in other program.
"
HOMEPAGE="https://xalan.apache.org/${S_PN}"
DOWNLOADS="mirror://apache/xalan/${S_PN}/source/${MY_PNV}-src.tar.gz"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/apidocs/index.html [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/readme.html"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

RESTRICT="test"

DEPENDENCIES="
    build+run:
        dev-java/bcel[>=5.2]
        dev-java/jakarta-regexp[>=1.5]
        dev-java/java_cup[>=0.11a]
        dev-java/JLex[>=1.2.6]
        dev-java/xml-commons-external[>=1.3.4]
"

ANT_BUNDLED_LIBSDIR=(
    tools
    lib
)

ANT_SRC_PREPARE_PARAMS=( clean )
ANT_SRC_COMPILE_PARAMS=( compile )
ANT_SRC_INSTALL_PARAMS=( unbundledjar )

WORK="${WORKBASE}/${MY_PNV}"

src_prepare() {
    ant_src_prepare

    local bcel_ver=$(best_version dev-java/bcel)
    local cup_ver=$(best_version dev-java/java_cup)
    local regexp_ver=$(best_version dev-java/jakarta-regexp)
    local xmlcomext_ver=$(best_version dev-java/xml-commons-external)
    bcel_ver=${bcel_ver%%:*}
    cup_ver=${cup_ver%%:*}
    cup_ver=${cup_ver/*\/}
    cup_ver=${cup_ver/0.}
    cup_ver=${cup_ver%%-p*}
    regexp_ver=${regexp_ver%%:*}
    xmlcomext_ver=${xmlcomext_ver%%:*}

    edo ln -s /usr/share/bcel/${bcel_ver/*\/}.jar "${WORK}"/lib/BCEL.jar
    edo ln -s /usr/share/jakarta-regexp/${regexp_ver/*\/}.jar "${WORK}"/lib/regexp.jar
    edo ln -s /usr/share/java_cup/${cup_ver/_/-}.jar "${WORK}"/tools/java_cup.jar
    edo ln -s /usr/share/JLex/JLex.jar "${WORK}"/tools/JLex.jar
    edo ln -s /usr/share/xml-commons-external/${xmlcomext_ver/*\/}.jar "${WORK}"/lib/xml-apis-ext.jar

    # The -static argument is not supported.
    edo sed -i -e 's:\(<arg line="\)-static \(.*\):\1\2:' build.xml
}

src_install() {
    ant_src_install
    edo mv "${ANT_BUILD_DIR}"/xalan-unbundled.jar "${ANT_BUILD_DIR}"/xalan.jar

    dodir /usr/share/${PN}
    insinto /usr/share/${PN}
    doins "${ANT_BUILD_DIR}"/serializer.jar "${ANT_BUILD_DIR}"/xalan.jar
}

